/*
 * SimpleHttpClient.h
 *
 *  Created on: Dec 10, 2011
 *      Author: cimadai
 */

#ifndef SIMPLEHTTPCLIENT_H_
#define SIMPLEHTTPCLIENT_H_

#include <iostream>
#include "uv.h"

#define CHUNKS_PER_WRITE  1
#define CHUNK_SIZE        10485760 /* 10 MB */

class SimpleHttpClient {
	std::string ipaddr;
	int port;

public:
	SimpleHttpClient(std::string ipaddr, int port);
	virtual ~SimpleHttpClient();

	static uv_buf_t alloc_cb(uv_handle_t* handle, size_t size);
	static void close_cb(uv_handle_t* handle);
	static void shutdown_cb(uv_shutdown_t* req, int status);
	static void read_cb(uv_stream_t* tcp, ssize_t nread, uv_buf_t buf);
	static void write_cb(uv_write_t* req, int status);
	static void connect_cb(uv_connect_t* req, int status);

	void get(std::string url);

	std::string sendBuffer;
	std::string header;
	std::string body;
	uv_buf_t buf;
};

#endif /* SIMPLEHTTPCLIENT_H_ */
