/*
 * HostResolver.cpp
 *
 *  Created on: Dec 10, 2011
 *      Author: cimadai
 */

#include "HostResolver.h"

HostResolver::HostResolver() {
}

HostResolver::~HostResolver() {
}

void HostResolver::getaddrinfo_cb(uv_getaddrinfo_t* handle, int status,
		struct addrinfo* res) {
	HostResolver* pResolver = static_cast<HostResolver*>(handle->data);

	if (status == 0) {
		// addrinfo 構造体中の sockaddr 構造体からアドレスの表記文字列を求める。
		char ip[NI_MAXSERV + 1] = { '\0' };

		// 数字表記の IP アドレスを求める。0.0.0.0
		if (getnameinfo(res->ai_addr, (int) res->ai_addrlen, ip, sizeof(ip), 0,
				0, NI_NUMERICHOST) == 0) {
			pResolver->ipaddr = std::string(ip);
		}
	}
	delete handle;
	uv_freeaddrinfo(res);
}

std::string HostResolver::resolve(std::string hostName) {
	uv_getaddrinfo_t* getaddrinfo_handle = new uv_getaddrinfo_t();
	getaddrinfo_handle->data = this;
	int r = uv_getaddrinfo(static_cast<uv_loop_s*>(uv_default_loop()), getaddrinfo_handle,
			&HostResolver::getaddrinfo_cb, hostName.c_str(), NULL, NULL);

	if (r == 0) {
		uv_run(static_cast<uv_loop_s*>(uv_default_loop()));
	}
	return ipaddr;
}
