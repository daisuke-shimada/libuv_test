/*
 * HostResolver.h
 *
 *  Created on: Dec 10, 2011
 *      Author: cimadai
 */

#ifndef HOSTRESOLVER_H_
#define HOSTRESOLVER_H_

#include <iostream>
#include "uv.h"

class HostResolver {
public:
	HostResolver();
	virtual ~HostResolver();

	int result;
	std::string ipaddr;


	std::string resolve(std::string hostName);

	static void getaddrinfo_cb(uv_getaddrinfo_t* handle, int status, struct addrinfo* res);
};

#endif /* HOSTRESOLVER_H_ */
