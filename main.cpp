/*
 * main.cpp
 *
 *  Created on: Dec 10, 2011
 *      Author: cimadai
 */

#include <iostream>
#include "SimpleHttpClient.h"
#include "HostResolver.h"

int main(int argc, char * argv[]) {
	if (argc < 4) {
		printf("usage: libuv_test host port url\n");
		return 0;
	}

	// ホスト名からIPアドレスに変換
	HostResolver resolver;
	std::string ipaddr = resolver.resolve(argv[1]);
	if (ipaddr == "") {
		printf("can't resolve host name.\n");
		return 0;
	}
	int port = atoi(argv[2]);
	std::string url = argv[3];

	SimpleHttpClient shc(ipaddr, port);
	shc.get(url);

	return 0;
}

