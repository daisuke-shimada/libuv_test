CC = g++
CFLAG = -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP
LDFLAGS =
INCLUDES = -I./include -I/opt/local/include
LIBS = -L./lib -luv
TARGET = libuv_test
OBJS = main.o SimpleHttpClient.o HostResolver.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

clean:
	-rm -f $(TARGET) $(OBJS) *.o *.d

.cpp.o:
	$(CC) $(CFLAG) $(INCLUDES) -c $<

main.o: SimpleHttpClient.h HostResolver.h

