/*
 * SimpleHttpClient.cpp
 *
 *  Created on: Dec 10, 2011
 *      Author: cimadai
 */

#include "SimpleHttpClient.h"
#include <boost/format.hpp>

SimpleHttpClient::SimpleHttpClient(std::string ipaddr, int port)
	: ipaddr(ipaddr), port(port) {
}

SimpleHttpClient::~SimpleHttpClient() {
}

uv_buf_t SimpleHttpClient::alloc_cb(uv_handle_t* handle, size_t size) {
	uv_buf_t buf;
	buf.base = new char[size];
	buf.len = size;
	return buf;
}

void SimpleHttpClient::close_cb(uv_handle_t* handle) {
	delete handle;
}

void SimpleHttpClient::shutdown_cb(uv_shutdown_t* req, int status) {
	delete req;
}

void SimpleHttpClient::read_cb(uv_stream_t* tcp, ssize_t nread, uv_buf_t buf) {
	SimpleHttpClient* self = static_cast<SimpleHttpClient*>(tcp->data);
	if (nread < 0) {
		// EOF
		if (buf.base) {
			delete buf.base;
		}

		uv_close((uv_handle_t*) tcp, SimpleHttpClient::close_cb);
		return;
	}

	if (self->header == "") {
		std::string buffer(buf.base);
		size_t pos = buffer.find("\r\n\r\n", 0);
		self->header = buffer.substr(0, pos);
		self->body = buffer.substr(pos + 4);
	} else {
		self->body += std::string(buf.base).substr(0, nread);
	}

	delete buf.base;
}

void SimpleHttpClient::write_cb(uv_write_t* req, int status) {
	if (status) {
		uv_err_t err = uv_last_error(static_cast<uv_loop_s*>(uv_default_loop()));
		fprintf(stderr, "\nuv_write error: %s\n", uv_strerror(err));
	}
	delete req;
}

void SimpleHttpClient::connect_cb(uv_connect_t* req, int status) {
	SimpleHttpClient* self = static_cast<SimpleHttpClient*>(req->data);
	uv_stream_t* tcp = req->handle;
	delete req;

	// 書き込みバッファ
	uv_buf_t buf;
	buf.len = self->sendBuffer.length();
	buf.base = const_cast<char*>(self->sendBuffer.c_str());

	// Writeリクエスト
	uv_write_t* write_req = new uv_write_t();
	write_req->data = self;

	int r = uv_write(write_req, tcp, &buf, 1, SimpleHttpClient::write_cb);

	// 読み込み開始
	tcp->data = self;
	r = uv_read_start(tcp, SimpleHttpClient::alloc_cb, SimpleHttpClient::read_cb);
}

/**
 * GETリクエストの発行
 */
void SimpleHttpClient::get(std::string url) {
	struct sockaddr_in addr = uv_ip4_addr(this->ipaddr.c_str(), this->port);
	uv_tcp_t* client = new uv_tcp_t();
	uv_connect_t* connect_req = new uv_connect_t();
	int r;

	// リクエストヘッダ
	this->sendBuffer = (
			boost::format("GET %s HTTP/1.0\r\nHost: %s:%d\r\n\r\n") %
			url % this->ipaddr % this->port).str();

	// 自分を参照する為のポインタをいれておく。
	connect_req->data = this;
	r = uv_tcp_init(static_cast<uv_loop_s*>(uv_default_loop()), client);
	r = uv_tcp_connect(connect_req, client, addr, SimpleHttpClient::connect_cb);
	uv_run(static_cast<uv_loop_s*>(uv_default_loop()));
	printf("***header = %s\n", header.c_str());
	printf("***body = %s\n", body.c_str());
	printf("***body_len = %d\n", static_cast<int>(body.length()));
}
